## Instruction to setup and start docker container aeronetlab/dev
### This container contains identical to aeronetlab/prod-gpu packages, but launches with jupyter server on

the Dockerfile can be used to create Docker container with all librares for DL analysis of aerial images.
run of the image will open jupyter notebook server automatically.  

**PASSWORD FOR JUPYTER NOTEBOOK**: aeronet15  

### Build image use (inside folder with Dockerfile):  
```bash
$ nvidia-docker build -t aeronetlab/dev:<tag> .
```

### Start a container as background process use:  
```bash
$ nvidia-docker run -d --rm \
    -p <jupyter_port>:8888 \
    -p <tensorboard_port>:6006 \
    --name $(whoami) \
    -v <your_folder>:<folder_in_container> \
    -e "UID=$(id -u)" \
    -e "GID=$(id -g)" \
    aeronetlab/dev:<tag>
```

Where:  

nvidia-docker : should be used for using GPU.  
-d : to start as daemon.  
-p : use your port on the host for jupyter notebook and tensorboard  
--name : type name of your container, or use $(whoami) to use your account name.  
-v : type here folder to mount in container. For example /home/main:/home/user/mounted_folder  
-e : use your user ID to solve permission problem, by leaving "UID=$(id -u)" and "GID=$(id -g)" your uid and gid will be set automatically.   
For fast start you have to change -v and -p arguments only.  

**IMPORTANT**: do not mount your home directory to docker user home directory!  

### Docker compose template
```yaml
version: '2.3'

services: 

  jupyter-lab:
    container_name: <name of container>

    image: aeronetlab/dev:<tag>
    ports:
      - <local_port>:8888 # notebook server
      - <local_port>:6006 # for tensorboard
    volumes:
      - <local/path>:/home/user/project
    environment:
      - UID=1000
      - GID=1000
    restart: always
    runtime: nvidia
    ipc: host

```

### Enter into container as root use:  
```bash
$ nvidia-docker exec -it  $(whoami) /bin/bash
```

### To install new packages use:  
1) enter to container as root  
2) use `apt-get` or `pip` to install package (e.g. `pip install keras`)

### Write permissions to files and folders  
IMPORTANT - if you created files by root and want to have permission do following:  
```bash
$ nvidia-docker exec -it  $(whoami) /bin/bash            # enter container
$ chown -R user:user1 .                                 # change permissions
```
