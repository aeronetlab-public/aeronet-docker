#!/bin/bash

# Modify user GID and UID
sudo usermod -g $GID user
sudo usermod -u $UID user

# Run jupyter by user
gosu user jupyter lab
