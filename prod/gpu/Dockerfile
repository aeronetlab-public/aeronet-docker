FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04

## Base packages for ubuntu

# clean the libs list
RUN apt-get clean
RUN apt-get update -qq
RUN apt-get install -y \
    git \
    wget \
    bzip2 \
    htop \
    vim \
    nano \
    g++ \
    make \
    build-essential \
    software-properties-common \
    apt-transport-https \
    sudo \
    gosu \
    libgl1-mesa-glx \
    graphviz

## Download and install miniconda

RUN wget https://repo.continuum.io/miniconda/Miniconda3-4.5.4-Linux-x86_64.sh -O /tmp/miniconda.sh

RUN /bin/bash /tmp/miniconda.sh -b -p /opt/conda && \
    rm /tmp/miniconda.sh && \
    echo "export PATH=/opt/conda/bin:$PATH" > /etc/profile.d/conda.sh

ENV PATH /opt/conda/bin:$PATH

## Install base conda packages

RUN conda install -y numpy==1.14.0 \
                     scipy==1.0.0 \
                     pandas==0.22.0 \
                     matplotlib==2.2.2 \
                     seaborn==0.8.1 \
                     pillow==5.0.0 \
                     scikit-learn==0.19.1 \
                     jupyter==1.0.0

## TF + Keras

RUN pip install https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.8.0-cp36-cp36m-linux_x86_64.whl
RUN pip install Keras==2.1.5

## Setup order is important (GDAL, Rasterio, OpenCV)! - otherwise it won't import due to dependency conflict 

## Install GDAL wrapper

RUN conda install -y -c conda-forge gdal==2.2.4

## Rasterio

RUN conda install -y -c conda-forge/label/dev rasterio==1.0b2

## OpenCV

RUN conda install -c conda-forge opencv==3.4.1 -yy

## Additional GIS-related packages etc.

RUN pip install shapely==1.6.4.post1 \
                tqdm==4.19.9 \
                h5py==2.8.0rc1 \
                scikit-image==0.13.1 \
                matplotlib==2.2.2 \
                pandas==0.22.0 \
                geojson==2.3.0 \
                tifffile==0.14.0 \
                graphviz==0.8.2 \
                pydot==1.2.3


## Setup Rtree and required native libspatialindex

### Build libspatialindex from source because conda's libspatialindex conflicts with GDAL 
RUN wget http://download.osgeo.org/libspatialindex/spatialindex-src-1.8.5.tar.gz -O /tmp/spatialindex-src.tar.gz && \
    tar -xvf /tmp/spatialindex-src.tar.gz -C /tmp
WORKDIR /tmp/spatialindex-src-1.8.5
RUN ./configure && make && make install && ldconfig
RUN pip install Rtree==0.8.3
